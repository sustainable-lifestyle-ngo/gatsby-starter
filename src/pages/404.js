import React from 'react';
import { injectIntl } from 'gatsby-plugin-intl';

import { Layout, SEO, TextBody } from 'components';

const NotFoundPage = ({ intl }) => {
  return (
    <Layout>
      <SEO title={intl.formatMessage({ id: 'notfound_title' })} />
      <TextBody>
        {intl.formatMessage({ id: 'notfound_text' })}{' '}
        <span role="img" aria-label="duh">
          😓
        </span>
      </TextBody>
    </Layout>
  );
};

export default injectIntl(NotFoundPage);
