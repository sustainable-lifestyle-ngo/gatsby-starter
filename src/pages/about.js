import React from 'react';
import styled from 'styled-components';
import { injectIntl } from 'gatsby-plugin-intl';

import { Layout, SEO } from 'components';

const TextHome = styled.p`
  max-width: 28em;
  text-align: center;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 10vh;

  font-size: 22px;
  line-height: 1.6;

  @media (max-width: 52em) {
    font-size: 19px;
  }
`;

const Home = ({ intl }) => {
  return (
    <Layout>
      <SEO title={intl.formatMessage({ id: 'about_title' })} />
      <TextHome>{intl.formatMessage({ id: 'about_text1' })}</TextHome>
    </Layout>
  );
};

export default injectIntl(Home);
