import React from 'react';
import styled from 'styled-components';
import { layout, flexbox, typography } from 'styled-system';

import { Footer, GlobalStyles, Header } from 'components';

const Content = styled.main`
  margin: 0 auto;
  flex: 1;
  ${layout}
  ${flexbox}
  ${typography}
`;

const Layout = ({ children, ...layoutProps }) => {
  return (
    <>
      <GlobalStyles />
      <Header />
      <Content {...layoutProps}>{children}</Content>
      <Footer />
    </>
  );
};

export default Layout;
