import React from 'react';
import styled from 'styled-components';
import { injectIntl } from 'gatsby-plugin-intl';

const Container = styled.footer`
  text-align: center;
  font-size: 18px;
  padding: 20px 0;
`;

const Link = styled.a`
  :hover {
    text-decoration: underline;
  }
`;

const Footer = ({ intl }) => {
  return (
    <Container>
      <div>
        2015 - {new Date().getFullYear()} ©{' '}
        {intl.formatMessage({ id: 'title' })}
      </div>
      <div>{intl.formatMessage({ id: 'rights' })}</div>
      <Link
        href="https://linkedin.com/in/gabriel-moncea/"
        target="_blank"
        rel="author"
        referrerpolicy="no-referrer"
      >
        {intl.formatMessage({ id: 'author' })}
      </Link>
    </Container>
  );
};

export default injectIntl(Footer);
