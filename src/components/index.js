import Footer from 'components/Footer';
import GlobalStyles from 'components/GlobalStyles';
import Header from 'components/Header';
import Layout from 'components/Layout';
import Nav from 'components/Nav';
import SEO from 'components/SEO';
import TextBody from 'components/TextBody';

export {
  Footer,
  GlobalStyles,
  Header,
  Layout,
  Nav,
  SEO,
  TextBody,
};
