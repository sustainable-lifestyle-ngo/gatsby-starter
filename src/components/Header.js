import React from 'react';
import styled from 'styled-components';
import { space } from 'styled-system';
import { Nav } from 'components';

const Bar = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 12vh;
  ${space}
`;

const urls = ['', 'about'];

const Header = () => (
  <Bar>
    <Nav urls={urls} />
  </Bar>
);

export default Header;
