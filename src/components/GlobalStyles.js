import { createGlobalStyle } from 'styled-components';
import * as font from 'fonts';

export default createGlobalStyle`
* {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

@font-face {
  font-family: "RobotoCondensed";
  src: url('${font.RobotoCondensed400}') format("ttf");
  font-weight: 400;
  font-style: normal;
  font-display: block;
}

@font-face {
  font-family: "RobotoCondensed";
  src: url('${font.RobotoCondensed700}') format("ttf");
  font-weight: 700;
  font-style: normal;
  font-display: block;
}

body {
  height: 100%;
  width: 100%;
  margin: 0;
  margin-left: auto;
  margin-right: auto;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  line-height: 1.5;
  font-size: 19px;
  font-family: RobotoCondensed;
  font-weight: 400;
  padding: 0 3vw;

  @media (max-width: 52em) {
    font-size: 17px;
  }
}

h1,
h2,
h3,
h4,
h5,
h6 {
  margin: 0;
  -webkit-margin-before: 0;
  margin-block-start: 0;
  -webkit-margin-after: 0;
  margin-block-end: 0;
  -webkit-margin-start: 0;
  margin-inline-start: 0;
  -webkit-margin-end: 0;
  margin-inline-end: 0;
  display: inline-block;
}

a {
  text-decoration: none;
  -webkit-tap-highlight-color: hsla(0, 0%, 0%, 0);
  -webkit-tap-highlight-color: transparent;
  color: inherit;
}

img {
  max-width: 100%;
  max-height: 100%;
  height: 100%;
  width: 100%;
}

html, body, #___gatsby {
  height: 100%;
}

div[role="group"][tabindex] {
  height: 100%;
  display: flex;
  flex-direction: column;
}
`;
